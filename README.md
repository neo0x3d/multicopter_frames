# multicopter_frames
This repository contains some experimental single/multicopter frames

* ball_drone
	Single or dual motor copter, surrounded by a protective cage.
	Similar to the Flyability drone, but with bent carbon fiber rods.
* cheap_bird
	Ulta low cost 3d printed or lasercut frame, assembly without screws.
